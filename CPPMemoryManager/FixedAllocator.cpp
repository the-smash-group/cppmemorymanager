#include "FixedAllocator.h"
#include <assert.h>

using namespace CoolKidzAllocators;

void Chunk::Init(std::size_t blockSize, unsigned char blocks)
{
	pData_ = new unsigned char[blockSize * blocks];
	firstAvailableBlock_ = 0;
	blocksAvailable_ = blocks;
	unsigned char i = 0;
	unsigned char* p = pData_;
	for (; i != blocks; p += blockSize)
	{
		*p = ++i;
	}
}

void* Chunk::allocate(std::size_t blockSize)
{
	if (!blocksAvailable_) return 0;
	unsigned char* pResult =
		pData_ + (firstAvailableBlock_ * blockSize);
	// Update firstAvailableBlock_ to point to the next block
	firstAvailableBlock_ = *pResult;
	--blocksAvailable_;
	return pResult;
}

void Chunk::deallocate(void* p, std::size_t blockSize)
{
	assert(p >= pData_);
	unsigned char* toRelease = static_cast<unsigned char*>(p);
	// Alignment check
	assert((toRelease - pData_) % blockSize == 0);
	*toRelease = firstAvailableBlock_;
	firstAvailableBlock_ = static_cast<unsigned char>(
		(toRelease - pData_) / blockSize);
	// Truncation check
	assert(firstAvailableBlock_ ==
		(toRelease - pData_) / blockSize);
	++blocksAvailable_;
}

void* FixedAllocator::allocate()
{
	if (allocChunk_ == 0 ||
		allocChunk_->blocksAvailable_ == 0)
	{
		// No available memory in this chunk
		// Try to find one
		Chunks::iterator i = chunks_.begin();
		for (;; ++i)
		{
			if (i == chunks_.end())
			{
				// All filled up-add a new chunk
				chunks_.reserve(chunks_.size() + 1);
				Chunk newChunk;
				newChunk.Init(blockSize_, numBlocks_);
				chunks_.push_back(newChunk);
				allocChunk_ = &chunks_.back();
				deallocChunk_ = &chunks_.back();
				break;
			}
			if (i->blocksAvailable_ > 0)
			{
				// Found a chunk
				allocChunk_ = &*i;
				break;
			}
		}
	}
	assert(allocChunk_ != 0);
	assert(allocChunk_->blocksAvailable_ > 0);
	return allocChunk_->allocate(blockSize_);
}

//1 Find the right chunk to deallocate starting by checking deallocChunk_
//2 if the pointer is not inside deallocChunk_ start searching the right chunk
//  with two iterators up and down from deallocChunk_
//3 if the chunk is empty after the deallocation move it at the end of the vector
//4 if there is already an empty chunk at the end of the vector delete it
void FixedAllocator::deallocate(void* p)
{
	assert(!chunks_.empty());
	assert(deallocChunk_ != 0);
	std::ptrdiff_t deallocChunkIndex = std::distance(chunks_.data(), deallocChunk_);
	if (p >= deallocChunk_->pData_ && p <= deallocChunk_->pData_ + blockSize_ * numBlocks_) {
		deallocChunk_->deallocate(p, blockSize_);
	}
	else {
		auto fwIterator = chunks_.begin() + deallocChunkIndex + 1;
		auto revIterator = chunks_.rend() - (deallocChunkIndex + 1) + 1;
		bool chunkFound = false;
		while (!chunkFound && (fwIterator != chunks_.end() || revIterator != chunks_.rend())) {
			if (fwIterator != chunks_.end()) {
				
				if (p >= fwIterator->pData_ && p <= fwIterator->pData_ + blockSize_ * numBlocks_) {
					chunkFound = true;
					deallocChunk_ = &*fwIterator;
					deallocChunk_->deallocate(p, blockSize_);
					break;
				}

				fwIterator++;
			}
			if (revIterator != chunks_.rend()) {
				
				if (p >= revIterator->pData_ && p <= revIterator->pData_ + blockSize_ * numBlocks_) {
					chunkFound = true;
					deallocChunk_ = &*revIterator;
					deallocChunk_->deallocate(p, blockSize_);
					break;
				}

				revIterator++;
			}

		}
		assert(chunkFound);
		deallocChunkIndex = std::distance(chunks_.data(), deallocChunk_);
	}
	if (deallocChunk_->blocksAvailable_ == numBlocks_) {
		if (&chunks_[deallocChunkIndex] != &chunks_.back()) {
			chunks_.push_back(chunks_[deallocChunkIndex]);
			chunks_.erase(chunks_.begin() + deallocChunkIndex);
			deallocChunk_ = &chunks_.back();
		}
		if (chunks_.size() > 1 && &chunks_.end()[-2] != nullptr && chunks_.end()[-2].blocksAvailable_ == numBlocks_) {
			Chunk& chunkToFree = chunks_.back();
			delete[] chunkToFree.pData_;
			chunkToFree.pData_ = nullptr;
			chunks_.pop_back();
			deallocChunk_ = &chunks_.back();
		}
		allocChunk_ = &chunks_.front();
	}
	return;
}
