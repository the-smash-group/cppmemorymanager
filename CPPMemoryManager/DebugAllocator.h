#pragma once

#include <iostream>
#include <map>

template<class WrappedAllocator>
class DebugAllocator : private WrappedAllocator
{
public:
	using WrappedAllocator::WrappedAllocator;

	inline void* allocate(std::size_t numBytes) {
		std::cout<<"Requested allocation of "<<numBytes<<"... ";
		void* p = WrappedAllocator::allocate(numBytes);
		std::cout<<"returned "<<p<<std::endl;

		allocationMap_[p] = numBytes;

		return p;
	}

	inline void deallocate(void* p, std::size_t size) {
		std::cout<<"Requested Allocation of address "<< p << " of size "<<size<<std::endl;
		WrappedAllocator::deallocate(p, size);
		allocationMap_.erase(p);
	}

	~DebugAllocator() {
		if (allocationMap_.size() > 0) {
			std::cout<<"DETECTED LEAKS: "<<std::endl<<"ADDRESS\tSIZE"<<std::endl;
			for (const pair tuple : allocationMap_) {
				std::cout<<tuple.first<<"\t"<<tuple.second<<std::endl;
			}
		}
	}

private:
	using map = std::map<void*, std::size_t>;
	using pair = map::value_type;

	map allocationMap_;
};
