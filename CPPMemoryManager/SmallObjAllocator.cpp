#include "SmallObjAllocator.h"

#include <algorithm>
#include <assert.h>

using namespace CoolKidzAllocators;



SmallObjAllocator::SmallObjAllocator(std::size_t chunkSize, std::size_t maxObjectSize) :
	chunkSize_(chunkSize), maxObjectSize_(maxObjectSize),
	pLastAlloc_(nullptr), pLastDealloc_(nullptr),
	pool_()
{}

void* SmallObjAllocator::allocate(std::size_t numBytes)
{
	//if numBytes > maxObjectSize allocate directly with ::operator new
	//Look for fixed allocator of requested size, and if you don't find it
	//Create it and add it to the pool_. pool_ should be then sorted.

	if (numBytes > maxObjectSize_) {
		return ::operator new(numBytes);
	}
	else if(pLastAlloc_ && numBytes == pLastAlloc_->GetBlockSize())
	{
		return pLastAlloc_->allocate();
	} 
	else 
	{
		FixedAllocator* RequestedAllocator = FindAllocator(numBytes);
		if (RequestedAllocator == nullptr)
		{
			FixedAllocator newAllocator(numBytes, static_cast<unsigned char>(chunkSize_));
			pool_.push_back(newAllocator);

			auto SortPredicate = [](const FixedAllocator& a, const FixedAllocator& b)->bool{return a.GetBlockSize() < b.GetBlockSize(); };
			std::sort(pool_.begin(), pool_.end(), SortPredicate);

			FixedAllocator* CreatedAllocator = FindAllocator(numBytes);
			pLastAlloc_ = CreatedAllocator;

			assert(CreatedAllocator != nullptr);
			return CreatedAllocator->allocate();
		}
		else {
			pLastAlloc_ = RequestedAllocator;
			return RequestedAllocator->allocate();
		}
	}
}

void SmallObjAllocator::deallocate(void* p, std::size_t size)
{
	if (size > maxObjectSize_) {
		delete p;
		return;
	}
	if (pLastDealloc_ && pLastDealloc_->GetBlockSize() == size) 
	{
		pLastDealloc_->deallocate(p);
	}
	else {
		FixedAllocator* RequestedAllocator = FindAllocator(size);
		assert(RequestedAllocator != nullptr);

		pLastDealloc_ = RequestedAllocator;
		RequestedAllocator->deallocate(p);
	}
}

FixedAllocator* SmallObjAllocator::FindAllocator(std::size_t numBytes)
{
	auto lookupPredicate = [numBytes](const FixedAllocator& a)->bool {return a.GetBlockSize() == numBytes; };
	auto SearchResult = std::find_if(pool_.begin(), pool_.end(), lookupPredicate);

	return SearchResult != pool_.end() ? &(*SearchResult) : nullptr;
}

