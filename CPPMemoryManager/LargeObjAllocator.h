#pragma once

#include "EASTL/internal/red_black_tree.h"

#include <new>
#include <assert.h>
#include <EASTL/hash_map.h>
#include <EASTL/set.h>

//Used by macros


void* operator new[](size_t size, const char* pName, int flags, unsigned debugFlags, const char* file, int line);
void* operator new[](size_t size, size_t alignment, size_t alignmentOffset, const char* pName, int flags, unsigned debugFlags, const char* file, int line);

namespace CoolKidzAllocators {
	struct Header {
		Header* _previous;
		std::size_t _size;

		inline Header(Header* previous, std::size_t size) : _previous(previous), _size(size) {}

		~Header() {}
	};

	struct MemoryBlock {
		unsigned char* _firstElement;
		std::size_t _size;

		inline MemoryBlock(unsigned char* firstElement, std::size_t size) : _firstElement(firstElement), _size(size){}

		inline bool contains(const unsigned char* const p) const { 
			return (p >= static_cast<unsigned char*>((void*)_firstElement) && p < (static_cast<unsigned char*>((void*)_firstElement)) + _size); 
		};

		~MemoryBlock() {
			delete[](_firstElement);
		}
	};

	template<int blocksToAllocate, std::size_t sizeTolerance>
	class LargeObjAllocator {
	public:
		using pair_type = eastl::pair<std::size_t, Header*>;
		using container_pair_type = eastl::pair<Header*, MemoryBlock*>;
		using tree_type = eastl::rbtree<std::size_t, pair_type, eastl::less<std::size_t>, EASTLAllocatorType, eastl::use_first<pair_type>, false, false>;
		using memory_block_container = eastl::hash_map<Header*, MemoryBlock*>;

		LargeObjAllocator();
		LargeObjAllocator(const LargeObjAllocator& other) = default;
		~LargeObjAllocator();

		void* allocate(size_t size);
		void deallocate(void* p, size_t size);

	protected:
		tree_type _tree;
		//needed for having contiuoos blocks in memory
		memory_block_container memoryBlockContainer;

		inline std::size_t _roundToHeader(std::size_t size) {
			constexpr size_t align = std::alignment_of<Header>::value;
			std::size_t roundedSize;
			if (size <= align) {
				return align;
			}
			if (size % align == 0) {
				return size;
			}
			else {
				std::size_t rem = size % align;
				roundedSize = size + align - rem;
				return roundedSize;
			}
		}

	};

	

/////////////////////////////////////////////////////////////////////////////
// TEMPLATE FUNCTION DEFINITIONS
/////////////////////////////////////////////////////////////////////////////

	template<int blocksToAllocate, std::size_t sizeTolerance>
	LargeObjAllocator<blocksToAllocate,sizeTolerance>::LargeObjAllocator() : _tree(), memoryBlockContainer(blocksToAllocate) {}

	//1) Check if there is an available free node in the red black tree
	//2) If it's available return it, if the remaning space + Headersize is >= sizeTolerance chop it before returning
	//3) Else allocate a new memory block
	//3.1) Chop the memory block in 2 pieces, one for the new object that request the allocation and the second for all the other space (if the remaning space + Headersize is >= sizeTolerance)
	template<int blocksToAllocate, std::size_t sizeTolerance>
	void* LargeObjAllocator<blocksToAllocate,sizeTolerance>::allocate(size_t size)
	{
		std::size_t roundedSize = _roundToHeader(size);
		tree_type::iterator Iterator = _tree.lower_bound(roundedSize);
		if (Iterator != _tree.end())
		{
			Header* foundHeader = nullptr;
			//WORST FIT
			Iterator = _tree.end();
			Iterator--;
			foundHeader = Iterator->second;
			assert(foundHeader != nullptr);
			assert(foundHeader->_size & 1);

			foundHeader->_size = roundedSize;

			if (Iterator->first - sizeof(Header) - roundedSize >= sizeTolerance) {

				unsigned char* newPosition = static_cast<unsigned char*>((void*)foundHeader) + sizeof(Header) + foundHeader->_size;
				Header* SecondHeader =new ((void*)newPosition) Header(foundHeader, Iterator->first - sizeof(Header) - roundedSize);

				_tree.insert(pair_type(SecondHeader->_size, SecondHeader));
				memoryBlockContainer.insert(
					container_pair_type(
						SecondHeader,
						memoryBlockContainer[Iterator->second]
					)
				);

				SecondHeader->_size |= 1;

			}
			else {
				foundHeader->_size = Iterator->first;
			}
			_tree.erase(Iterator);
			return(foundHeader + 1);


		}

		//Allocated size must be rounded to allow natural alignment of header
		std::size_t roundedMemoryBlockSize = roundedSize * blocksToAllocate;
		unsigned char* FullAllocation = new unsigned char[roundedMemoryBlockSize];
		assert(FullAllocation != nullptr);

		Header* HeaderPtr = new((void *)FullAllocation) Header(nullptr, roundedSize);
		MemoryBlock* memBlock = new MemoryBlock(FullAllocation, roundedMemoryBlockSize);
		memoryBlockContainer.insert(container_pair_type(HeaderPtr, memBlock));
		
		if (roundedMemoryBlockSize - 2*sizeof(Header) - roundedSize >= sizeTolerance) {
			
			unsigned char* newPosition = static_cast<unsigned char*>((void *)HeaderPtr) + sizeof(Header) + HeaderPtr->_size;
			Header* SecondHeader = new ((void *)newPosition) Header(HeaderPtr, roundedMemoryBlockSize - 2 * sizeof(Header) - roundedSize);
			memoryBlockContainer.insert(container_pair_type(SecondHeader, memBlock));

			_tree.insert(pair_type(SecondHeader->_size, SecondHeader));

			//this is used to mark as free the block associated to the header
			SecondHeader->_size |= 1;
		}
		return (HeaderPtr +1);
	}


	template<int blocksToAllocate, std::size_t sizeTolerance>
	void LargeObjAllocator<blocksToAllocate,sizeTolerance>::deallocate(void* p, size_t size)
	{
		Header* pointerToInsertInTheTree = static_cast<Header*>(p) - 1;
		std::size_t sizeToInsertInTheTree = pointerToInsertInTheTree->_size;
		MemoryBlock* toInsertInTheContainer = memoryBlockContainer[pointerToInsertInTheTree];
		assert(pointerToInsertInTheTree->_size >= size);

		//previusHeader
		Header* previusHeader = pointerToInsertInTheTree->_previous;
		//NextHeader
		unsigned char* pointerToNextHeader = static_cast<unsigned char*>((void*)pointerToInsertInTheTree) + pointerToInsertInTheTree->_size + sizeof(Header);

		//check if there's an header on pointerToNextHeader
		MemoryBlock* memoryBlock = memoryBlockContainer[pointerToInsertInTheTree];
		if (memoryBlock->contains(pointerToNextHeader)) {
			Header* nextHeader = static_cast<Header*>((void*)pointerToNextHeader);
			if ((nextHeader->_size & 1)) {
				//-1 because the last bit is 1 for marking it free
				sizeToInsertInTheTree = pointerToInsertInTheTree->_size + sizeof(Header) + nextHeader->_size - 1;
				memoryBlockContainer.erase(nextHeader);

				auto treeIterator = _tree.begin();
				while (treeIterator != _tree.end()) {
					if (treeIterator->first == nextHeader->_size - 1 && treeIterator->second == nextHeader)
						break;
					else
						treeIterator++;
				}
				assert(treeIterator != _tree.end());
				_tree.erase(treeIterator);
			}
		}
		
		memoryBlockContainer.erase(pointerToInsertInTheTree);

		if (previusHeader != nullptr && (previusHeader->_size & 1)) {
			sizeToInsertInTheTree = previusHeader->_size -1 + sizeof(Header) + sizeToInsertInTheTree;
			pointerToInsertInTheTree = previusHeader;
			auto treeIterator = _tree.begin();
			while (treeIterator != _tree.end()) {
				if (treeIterator->first == previusHeader->_size - 1 && treeIterator->second == previusHeader)
					break;
				else
					treeIterator++;
			}
			assert(treeIterator != _tree.end());
			_tree.erase(treeIterator);
			memoryBlockContainer.erase(previusHeader);

		}

		pointerToInsertInTheTree->_size = sizeToInsertInTheTree;
		unsigned char* NewNextHeader = static_cast<unsigned char*>((void*)pointerToInsertInTheTree) + sizeof(Header) + sizeToInsertInTheTree;
		if (toInsertInTheContainer->contains(NewNextHeader)) {
			//Update the "previous" pointer of the "new" next node, next to the coalesced one 

			/* 
			* The Coalesced One, Annihilator of Headers
			* 
			* ##############################################################
			* #|======================================                    |#
			* ##############################################################
			* 
			*/

			Header* AsHeader = static_cast<Header*>((void*)NewNextHeader);
			AsHeader->_previous = pointerToInsertInTheTree;
		}

		pointerToInsertInTheTree->_size |= 1;
		_tree.insert(pair_type(sizeToInsertInTheTree, pointerToInsertInTheTree));
		memoryBlockContainer.insert(container_pair_type(pointerToInsertInTheTree, toInsertInTheContainer));

	}

	template <typename T>
	class LargeObjAllocatorSTL {
	public:

		using value_type = T;
		using pointer = T*;
		using const_pointer = const T*;
		using reference = T&;
		using const_reference = const T&;
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;
		template<typename U>
		struct rebind
		{
			typedef LargeObjAllocatorSTL<U> other;
		};
		using propagate_on__container_move_assignment = std::true_type;
		using is_always_equal = std::true_type;

		LargeObjAllocatorSTL() = default;
		template<class U>
		LargeObjAllocatorSTL(const LargeObjAllocatorSTL<U>& other) {};

		static LargeObjAllocator<100, 2 * sizeof(T)> bigObjAll;

		[[nodiscard]] inline pointer allocate(size_type count) noexcept {
			return (pointer)bigObjAll.allocate(count * sizeof(T));
		}
		void inline deallocate(pointer p, size_type count) noexcept {
			bigObjAll.deallocate(p, count * sizeof(T));
		}
	};

	template<typename T>
	LargeObjAllocator<100, 2 * sizeof(T)> LargeObjAllocatorSTL<T>::bigObjAll = LargeObjAllocator<100, 2*sizeof(T)>();

	template<typename T, typename U>
	inline bool operator==(const LargeObjAllocatorSTL<T>& right, const LargeObjAllocatorSTL<U>& left) {
		return true;
	}

	template<typename T, typename U>
	inline bool operator!=(const LargeObjAllocatorSTL<T>& right, const LargeObjAllocatorSTL<U>& left) {
		return !(right == left);
	}
}

template<int blocksToAllocate, std::size_t sizeTolerance>
CoolKidzAllocators::LargeObjAllocator<blocksToAllocate, sizeTolerance>::~LargeObjAllocator()
{
	eastl::set<MemoryBlock*> toDelete; 
	for (container_pair_type mBlock : memoryBlockContainer) {
		toDelete.insert(mBlock.second);
	}

	for (MemoryBlock* mBlock : toDelete) {
		delete mBlock;
	}
}




namespace {
	static CoolKidzAllocators::LargeObjAllocator<10, 32> loa;
}

#define LOA_NEW(x) loa.allocate(x);
#define LOA_DELETE(x) loa.deallocate(x, 0);
