#include <iostream>
#include <chrono>
#include <EASTL/internal/red_black_tree.h>
#include <vector>

#include "SmallObjAllocator.h"
#include "DebugAllocator.h"
#include "LargeObjAllocator.h"
#include "SegregatorAlloc.h"

using namespace CoolKidzAllocators;
using Allocator = DebugAllocator<SmallObjAllocator>;

void FixedAllocatorTesting() {
	auto fixedAllocatorTest = FixedAllocator(sizeof(int), 2);
	int* elementi[9];
	for (int i = 0; i < 9; i++) {
		elementi[i] = static_cast<int*> (fixedAllocatorTest.allocate());
		*elementi[i] = i + 1;
	}

	fixedAllocatorTest.deallocate(elementi[8]);
	fixedAllocatorTest.deallocate(elementi[0]);
	fixedAllocatorTest.deallocate(elementi[4]);
	fixedAllocatorTest.deallocate(elementi[5]);
	fixedAllocatorTest.deallocate(elementi[3]);
	fixedAllocatorTest.deallocate(elementi[1]);
	fixedAllocatorTest.deallocate(elementi[2]);
	int* numeroACaso = static_cast<int*> (fixedAllocatorTest.allocate());
	*numeroACaso = 666;
	int* numeroACaso2 = static_cast<int*> (fixedAllocatorTest.allocate());
	*numeroACaso2 = 42;
	int* numeroACaso3 = static_cast<int*> (fixedAllocatorTest.allocate());
	*numeroACaso3 = 69;
	int* numeroACaso4 = static_cast<int*> (fixedAllocatorTest.allocate());
	*numeroACaso4 = 420;
}

void SmallAllocTesting() {
	Allocator mySmallObjectAllocator(sizeof(int) * 2, sizeof(int)*4);

	char* alloc1Bytes = (char*) mySmallObjectAllocator.allocate(1);
	*alloc1Bytes = 'c';
	char* alloc2Bytes = (char*) mySmallObjectAllocator.allocate(2);
	alloc2Bytes[0] = 'i';
	alloc2Bytes[1] = 'a'; 
	char* alloc2Bytes2 = (char*)mySmallObjectAllocator.allocate(2);
	alloc2Bytes2[0] = 'l';
	alloc2Bytes2[1] = 'b';
	int* allocAnInt = (int*) mySmallObjectAllocator.allocate(sizeof(int));
	*allocAnInt = 42069;
	int* allocAnotherInt = (int*) mySmallObjectAllocator.allocate(sizeof(int));
	*allocAnotherInt = 69420;
	char* alloc32Bytes = (char*) mySmallObjectAllocator.allocate(32);
	std::fill(alloc32Bytes, alloc32Bytes + 32, 63);

	mySmallObjectAllocator.deallocate(allocAnInt, sizeof(int));
	mySmallObjectAllocator.deallocate(allocAnotherInt, sizeof(int));
}

void LargeAllocTesting() {
	using namespace CoolKidzAllocators;
	
	LargeObjAllocator<10,32> loa;
	void* ptr = loa.allocate(31);
	for (int i = 0; i < 32; i++) {
		((char*)ptr)[i] = 'a';
	}

	void* ptr2 = loa.allocate(35);
	for (int i = 0; i < 36; i++) {
		((char*)ptr2)[i] = 'b';
	}

	loa.deallocate(ptr2, 35);

	void* ptr3 = loa.allocate(48);
	for (int i = 0; i < 48; i++) {
		((char*)ptr3)[i] = '3';
	}

	loa.deallocate(ptr,31);

	void* ptr5 = loa.allocate(8);
	for (int i = 0; i < 8; i++) {
		((char*)ptr5)[i] = '1';
	}

	void* ptr6 = loa.allocate(99);
	for (int i = 0; i < 99; i++) {
		((char*)ptr6)[i] = 'K';
	}

	void* ptr8 = loa.allocate(10);
	for (int i = 0; i < 10; i++) {
		((char*)ptr8)[i] = 'i';
	}

	void* ptr9 = loa.allocate(13);
	for (int i = 0; i < 13; i++) {
		((char*)ptr9)[i] = '�';
	}

	void* ptr10 = loa.allocate(13);
	for (int i = 0; i < 13; i++) {
		((char*)ptr10)[i] = 'u';
	}

	void* ptr11 = loa.allocate(13);
	for (int i = 0; i < 16; i++) {
		((char*)ptr11)[i] = '�';
	}

	loa.deallocate(ptr11, 1);
	loa.deallocate(ptr3, 1);
	void* ptr69 = loa.allocate(64);
	for (int i = 0; i < 64; i++) {
		((char*)ptr69)[i] = 'J';
	}
}

void PerformanceTest() {
	
	
	constexpr int testSize = 1000;

	SmallObjAllocator smolObjAll(UCHAR_MAX, sizeof(int));

	
	void* newTestInts[testSize];
	auto StartNew = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < testSize; i++) {
		newTestInts[i] = new int;
	}
	for (int i = 0; i < testSize; i++) {
		delete(newTestInts[i]);
	}

	auto EndNew = std::chrono::high_resolution_clock::now();
	auto NewElapsedTime = std::chrono::high_resolution_clock::duration(EndNew-StartNew).count();

	auto StartSOA = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < testSize; i++)
	{
		newTestInts[i] = (smolObjAll.allocate(sizeof(int)));
	}
	for (int i = 0; i < testSize; i++)
	{
		smolObjAll.deallocate(newTestInts[i], sizeof(int));
	}
	auto EndSOA = std::chrono::high_resolution_clock::now();
	auto SOAElapsedTime = std::chrono::high_resolution_clock::duration(EndSOA - StartSOA).count();

	std::cout<<"Time for "<<testSize<<" allocations and deallocations with new: "<<NewElapsedTime<< " ns" << std::endl;
	std::cout<<"Time for "<<testSize<<" allocations and deallocations with Small Object Allocator: "<<SOAElapsedTime<< " ns" << std::endl;
}

void LargeObjPerformanceTest() {
	constexpr std::size_t AllocSize = (std::size_t) 1e+7;
	constexpr std::size_t NumberOfAlloc= 100;
	void* Allocs[NumberOfAlloc];

	std::cout<<"Allocating and deallocating "<<AllocSize<<" bytes "<<NumberOfAlloc<<" times with new"<<std::endl;

	auto StartNew = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < NumberOfAlloc; i++) {
		Allocs[i] = new unsigned char[AllocSize];
	}
	for (int i = 0; i < NumberOfAlloc; i++) {
		delete[] Allocs[i];
	}
	auto EndNew = std::chrono::high_resolution_clock::now();
	auto NewElapsedTime = std::chrono::high_resolution_clock::duration(EndNew - StartNew).count();

	std::cout << "Allocating and deallocating " << AllocSize << " bytes " << NumberOfAlloc << " times with LOA" << std::endl;
	CoolKidzAllocators::LargeObjAllocator<NumberOfAlloc/10, AllocSize> loa;

	auto StartLOA = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < NumberOfAlloc; i++) {
		Allocs[i] = loa.allocate(AllocSize);
	}
	for (int i = 0; i < NumberOfAlloc; i++) {
		loa.deallocate(Allocs[i], AllocSize);
	}
	auto EndLOA = std::chrono::high_resolution_clock::now();
	auto LOAElapsedTime = std::chrono::high_resolution_clock::duration(EndLOA - StartLOA).count();

	std::cout << "Time for " << NumberOfAlloc << " allocations and deallocations with new: " << NewElapsedTime << " ns" << std::endl;
	std::cout << "Time for " << NumberOfAlloc << " allocations and deallocations with Large Object Allocator: " << LOAElapsedTime << " ns" << std::endl;
}

void TestMacros() {
	void* p = LOA_NEW(89);
	memcpy(p, "Questo e' stato allocato col grande allocatore per oggetti grandi dei CoolKidsIndustries", 89);
	std::cout << (char*) p<<std::endl;
	LOA_DELETE(p);

	
	std::cout<<"Alloco degli int con small object allocator"<<std::endl;
	int* i = (int*)SOA_NEW(10 * sizeof(int));
	for (int c = 0; c < 10; c++) {
		i[c] = c+1; 
		std::cout<<i[c]<<" ";
	}
	std::cout<<std::endl;
	SOA_DELETE(i, 10 * sizeof(int));
}

void TestSTLAllocatorz() {

	std::vector<int, SmallObjAllocatorSTL<int>> vectorOfLittleInts = {2, 3, 5, 5 ,6, 9, 59, 69, 420, 13, 666, 777};

	for (int elem : vectorOfLittleInts) {
		std::cout << elem << " ";
	}

	std::cout << "\n";

	std::vector<int64_t, LargeObjAllocatorSTL<int64_t>> vectorOfBigInts = { 111111111,22222222,33333333,444444444, INT64_MAX };
	for (int64_t elem : vectorOfBigInts) {
		std::cout << elem << " ";
	}

	std::cout << "\n";

}

void SegregatorTests() {
	Segregator<SmallObjAllocator, LargeObjAllocator<10, 128>, 128> segregator;

	char* p1 = (char*) segregator.allocate(256);

	char* p2 = (char*) segregator.allocate(8);

	segregator.deallocate(p2, 8);
	segregator.deallocate(p1, 256);
}

int main()
{
	std::cout<<"=====STARTED TESTING====="<<std::endl;
	std::cout << "=====FIXED ALLOCATOR TESTING STARTED=====" << std::endl;
	FixedAllocatorTesting();
	std::cout << "=====FIXED ALLOCATOR TESTING FINISHED=====" <<std::endl<<"=====SMALL OBJECT ALLOCATOR TESTING STARTED=====" << std::endl;
	SmallAllocTesting();
	std::cout << "=====SMALL OBJECT ALLOCATOR TESTING FINISHED=====" << std::endl << "=====ALL TESTS FINISHED=====" << std::endl;
	
	std::cout << "=====PERFORMANCE TEST=====" << std::endl;
	PerformanceTest();
	std::cout << "=====PERFORMANCE TEST FINISHED=====" << std::endl;

	std::cout << "=====LARGE OBJ ALLOCATOR TESTS=====" << std::endl;
	LargeAllocTesting();
	std::cout << "=====LARGE OBJ ALLOCATOR TESTING COMPLETE=====" << std::endl;

	std::cout << "=====LARGE OBJ PERFORMANCE TEST=====" << std::endl;
	LargeObjPerformanceTest();
	std::cout << "=====LARGE OBJ PERFORMANCE TEST FINISHED=====" << std::endl;
	
	std::cout << "=====TESTING MACROS=====" << std::endl;
	TestMacros();
	std::cout << "=====TESTING MACROS FINISHED=====" << std::endl;

	std::cout << "=====TESTING STL ALLOCATORZ=====" << std::endl;
	TestSTLAllocatorz();
	std::cout << "=====TESTING STL ALLOCATORZ FINISHED=====" << std::endl;

	std::cout << "=====TESTING SEGREGATOR=====" << std::endl;
	SegregatorTests();
	std::cout << "=====TESTING SEGREGATOR FINISHED=====" << std::endl;
}
