#pragma once

#include <iostream>
#include <vector>

namespace CoolKidzAllocators {

	struct Chunk
	{
		void Init(std::size_t blockSize, unsigned char blocks);
		void* allocate(std::size_t blockSize);
		void deallocate(void* p, std::size_t blockSize);
		unsigned char* pData_;
		unsigned char
			firstAvailableBlock_,
			blocksAvailable_;

	};

	class FixedAllocator
	{

	public:
		FixedAllocator(
			std::size_t blockSize,
			unsigned char numBlocks
		)
			: blockSize_(blockSize)
			, numBlocks_(numBlocks)
		{
			chunks_ = {};
			allocChunk_ = 0;
			deallocChunk_ = 0;
		}
		FixedAllocator(const FixedAllocator& other) = default;

		void* allocate();
		void deallocate(void* p);

		inline std::size_t GetBlockSize() const { return blockSize_; }

	private:
		std::size_t blockSize_;
		unsigned char numBlocks_;
		typedef std::vector<Chunk> Chunks;
		Chunks chunks_;
		Chunk* allocChunk_;
		Chunk* deallocChunk_;
	};

}
