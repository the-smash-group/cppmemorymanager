#pragma once

#include <memory>
#if _DEBUG
#include <iostream>
#endif
/*
* 
* *************************************************************
* *             ~B~O~N~U~S~ ~A~L~L~O~C~A~T~O~R~               *
* *************************************************************
* 
*/

namespace CoolKidzAllocators
{
	template<class AllocSmall, class AllocBig, std::size_t Threshold = 128>
	class Segregator : public AllocSmall,public AllocBig
	{
	public:
		Segregator() : AllocSmall(), AllocBig() {};
		~Segregator() = default;

		void* allocate(std::size_t size)
		{
			if (size > Threshold)
			{
				#if _DEBUG
				std::cout<<"Allocating with Big Allocator"<<std::endl;
				#endif
				return AllocBig::allocate(size);
			}
			else
			{
				#if _DEBUG
				std::cout << "Allocating with Small Allocator" << std::endl;
				#endif
				return AllocSmall::allocate(size);
			}
		}

		void deallocate(void* ptr, std::size_t size)
		{
			if (size > Threshold)
			{
				#if _DEBUG
				std::cout << "Deallocating with Big Allocator" << std::endl;
				#endif
				return AllocBig::deallocate(ptr, size);
			}
			else
			{
				#if _DEBUG
				std::cout << "Deallocating with Small Allocator" << std::endl;
				#endif
				return AllocSmall::deallocate(ptr, size);
			}
		}
	};
}