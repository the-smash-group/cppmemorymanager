#pragma once

#include "FixedAllocator.h"
#include <iostream>
#include <vector>
#include <memory>

namespace CoolKidzAllocators {

class SmallObjAllocator
{
public:
	inline SmallObjAllocator() : SmallObjAllocator(255, 32) {}

	SmallObjAllocator(
		std::size_t chunkSize,
		std::size_t maxObjectSize);

	SmallObjAllocator(const SmallObjAllocator& other) noexcept {
		maxObjectSize_ = other.maxObjectSize_;
		chunkSize_ = other.chunkSize_;
		pLastAlloc_ = other.pLastAlloc_;
		pLastDealloc_ = other.pLastDealloc_;
	};

	SmallObjAllocator& operator=(const SmallObjAllocator& other) noexcept {
		if(this != &other)
		{
			maxObjectSize_ = other.maxObjectSize_;
			chunkSize_ = other.chunkSize_;
			pLastAlloc_ = other.pLastAlloc_;
			pLastDealloc_ = other.pLastDealloc_;
		}
		return *this;
	}

	void* allocate(std::size_t numBytes);
	void deallocate(void* p, std::size_t size);

protected:
	std::size_t maxObjectSize_;
	std::size_t chunkSize_;

	std::vector<FixedAllocator> pool_;
	FixedAllocator* pLastAlloc_;
	FixedAllocator* pLastDealloc_;

	//Utilities
	FixedAllocator* FindAllocator(std::size_t numBytes);
};

template <typename T>
class SmallObjAllocatorSTL {
public:
	using value_type = T;
	using pointer = T*;
	using const_pointer = const T*;
	using reference = T&;
	using const_reference = const T&;
	using size_type = std::size_t;

	template<typename U>
	struct rebind
	{
		typedef SmallObjAllocatorSTL<U> other;
	};

	using difference_type = std::ptrdiff_t;
	using propagate_on__container_copy_assignment = std::true_type;
	using propagate_on__container_move_assignment = std::true_type;
	using is_always_equal = std::true_type;


	SmallObjAllocatorSTL() {}
	
	template<typename U>
	SmallObjAllocatorSTL(const SmallObjAllocatorSTL<U>& other_alloc) {}

	[[nodiscard]] inline pointer allocate(size_type count) noexcept {
		return (pointer)smallObjAlloc.allocate(sizeof(T) * count);
	}
	void inline deallocate(pointer p, size_type count) noexcept {
		smallObjAlloc.deallocate(p, count * sizeof(T));
	}

	static SmallObjAllocator smallObjAlloc;
};

	template<typename T>
	SmallObjAllocator SmallObjAllocatorSTL<T>::smallObjAlloc(255, 16 * sizeof(T));

	template<typename T, typename U>
	inline bool operator==(const SmallObjAllocatorSTL<T>& right, const SmallObjAllocatorSTL<U>& left) {
		return false;
	}

	template<typename T, typename U>
	inline bool operator!=(const SmallObjAllocatorSTL<T>& right, const SmallObjAllocatorSTL<U>& left) {
		return !(right == left);
	}

}


namespace {
	static CoolKidzAllocators::SmallObjAllocator soa = CoolKidzAllocators::SmallObjAllocator(255, 16);
}
#define SOA_NEW(size) soa.allocate(size);
#define SOA_DELETE(p, size) soa.deallocate(p, size);